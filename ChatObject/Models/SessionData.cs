﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatObject.Models
{
    public class SessionData
    {
        /// <summary>
        /// The id of the user in this session
        /// </summary>
        [RequiredAttribute]
        public string? accountId { get; set; }

        /// <summary>
        /// The unique token for this session.
        /// </summary>
        [RequiredAttribute]
        public string? token { get; set; }

        /// <summary>
        /// The name of the current user
        /// </summary>
        public string? client_name { get; set; }

        /// <summary>
        /// The segment of the user
        /// </summary>
        public string? client_segment { get; set; }

        /// <summary>
        /// The current context of the user with Watson
        /// </summary>
        [RequiredAttribute]
        public Dictionary<string, object>? context { get; set; }

        /// <summary>
        /// The id of conversation for marking each interaction
        /// </summary>
        [RequiredAttribute]
        public string? mongo_id { get; set; }

        /// <summary>
        /// The ip address of the requests
        /// </summary>
        public string? ip_address { get; set; }

        /// <summary>
        /// The page which the user uses to contact with the chatbot
        /// </summary>
        public string? referrer { get; set; }

        /// <summary>
        /// The id of the assistant session
        /// </summary>
        public string? assistantSession { get; set; }

        /// <summary>
        /// The language selected by the user
        /// </summary>
        public string language { get; set; } = "es";
    }
}
