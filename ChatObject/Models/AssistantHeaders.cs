﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatObject.Models
{
    public class AssistantHeaders
    {
        /// <summary>
        /// The current account id of the session
        /// </summary>
        public string x_account_id { get; set; }
    }
}
