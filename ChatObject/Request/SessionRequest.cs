﻿namespace ChatObject.Request
{
    public class SessionRequest
    {
        /// <summary>
        /// The user id generated from the widget to achieve requests of messages,
        /// this keeps the session for this user against Watson Assistant
        /// </summary>
        public string? accountId { get; set; }

        /// <summary>
        /// The user id generated from the widget to achieve requests of messages,
        /// this keeps the session for this user against Mongo
        /// </summary>
        public string? userId { get; set; }

        /// <summary>
        /// The name of the user that is having the conversation with the chatbot
        /// </summary>
        public string? client_name { get; set; }

        /// <summary>
        /// The segment that the client in enrolled
        /// </summary>
        public string? client_segment { get; set; }

        /// <summary>
        /// The page where the user was when started the conversation
        /// </summary>
        public string? referrer { get; set; }

        /// <summary>
        /// The browser that the user is using
        /// </summary>
        public string? browser { get; set; }

        /// <summary>
        /// The OS that the user is using
        /// </summary>
        public string? os { get; set; }

        /// <summary>
        /// The type of device of the user (MOBILE/DESKTOP)
        /// </summary>
        public string? device { get; set; }

        /// <summary>
        /// The type of language selected by the user
        /// </summary>
        public string? language { get; set; }
    }
}
