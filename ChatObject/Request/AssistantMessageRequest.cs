﻿namespace ChatObject.Request
{
    public class AssistantMessageRequest
    {
        /// <summary>
        /// The message of the user
        /// </summary>
        public string? message { get; set; }

        /// <summary>
        /// The message of the user
        /// </summary>
        public string? audio { get; set; }
    }
}
