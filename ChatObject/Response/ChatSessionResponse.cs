﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatObject.Response
{
    public class ChatSessionResponse
    {
        /// <summary>
        /// The status of the user
        /// </summary>
        public string? status { get; set; }

        /// <summary>
        /// The inside data of the session
        /// </summary>
        public AccountProperties? account { get; set; }
    }

    public class AccountProperties
    {
        /// <summary>
        /// The name of the current user
        /// </summary>
        public string? client_name { get; set; }

        /// <summary>
        /// The segment of the user
        /// </summary>
        public string? client_segment { get; set; }

        /// <summary>
        /// The ip address of the requests
        /// </summary>
        public string? ip_address { get; set; }

        /// <summary>
        /// The page which the user uses to contact with the chatbot
        /// </summary>
        public string? referrer { get; set; }

        /// <summary>
        /// The token of the user
        /// </summary>
        public string? token { get; set; }
    }
}
