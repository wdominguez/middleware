﻿namespace ChatObject.Response
{
    public class AssistantMessageResponse
    {
        /// <summary>
        /// The response of the endpoint
        /// </summary>
        [Required]
        public List<MessageTextAudio>? response { get; set; }

        /// <summary>
        /// The list of intent returned by the Assistant
        /// </summary>
        [Required]
        public List<RuntimeIntent>? intents { get; set; }

        /// <summary>
        /// The list of entities returned by the Assistant
        /// </summary>
        [Required]
        public List<RuntimeEntity>? entities { get; set; }

        /// <summary>
        /// The current parsed user context returned by the assistant
        /// </summary>
        [Required]
        public Dictionary<string, object>? context { get; set; }

        public string? location { get; set; }
        public string? keys { get; set; }
    }
}
