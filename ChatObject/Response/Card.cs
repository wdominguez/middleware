﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatObject.Response
{
    public class Card
    {
        public string? title { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }
        public string? description { get; set; }
        public string? image_url { get; set; }
        public string? url { get; set; }
    }
}
