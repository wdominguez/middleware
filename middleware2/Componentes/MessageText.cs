﻿using ChatObject;
using IBM.Cloud.SDK.Core.Http;

namespace middleware2.Componentes
{
    public class MessageText
    {
        public async Task<AssistantMessageResponse> MessageT(IConfiguration configuration, IRepositorio repositorio) 
        {
            try
            {
                //string url = String.Format(configuration.GetConnectionString("nds") + "WeatherForecast", "es");
                //var responseHttp = await repositorio.Get<string>(url);
                //var reponseHttp = await repositorio.Post<ModelBusquedaDatos, List<ModeloBusqueda>>(configuration["UrlApiGateway"] + "Busqueda/", model, strToken);

                //clase1 ww = new clase1();
                //ww.nombre = "wqq";
                //ww.edad = 1;
                //string url = String.Format(configuration.GetConnectionString("nds") + "api/Prueba/RespuestaP", "es");
                //var res = await repositorio.Post<clase1, clase1>(url, ww);

                AssistantMessageResponse parsedResponse = new AssistantMessageResponse();
                parsedResponse.response = new List<MessageTextAudio>() { new MessageTextAudio() { text = "<p><b>ejemplo Formato HTML</b> </p><p>prueba1 <u>Subrrayado</u> trxto normal</p><p>Ejemplo link: <a href='#testLink'>haz clic</a> Texto normal  <ol><li>Lista 1</li><li>Lista 2</li><li>Lista 3</li></ol>fin de texto</p>" } };
                parsedResponse.intents = new List<RuntimeIntent>() { new RuntimeIntent() { Intent = "inf_gral", Confidence = 1 } };
                parsedResponse.context = new Dictionary<string, object>() { };



                parsedResponse.context.Add("agente", false);
                parsedResponse.context.Add("cont", 2);
                parsedResponse.context.Add("text", "");
                parsedResponse.context.Add("today", "Miercoles");
                parsedResponse.context.Add("buscar", null);
                parsedResponse.context.Add("images", null);
                parsedResponse.context.Add("remesa", null);
                parsedResponse.context.Add("estados", null);
                parsedResponse.context.Add("new_url", null);
                parsedResponse.context.Add("loc_pass", null);
                parsedResponse.context.Add("opciones", null);
                parsedResponse.context.Add("despedida", null);
                parsedResponse.context.Add("localizar", null);
                parsedResponse.context.Add("confidence", 0.3);
                parsedResponse.context.Add("iniciativa", 30);
                parsedResponse.context.Add("time_query", null);
                parsedResponse.context.Add("duda_generica", null);
                parsedResponse.context.Add("tipos_tarjeta", null);
                parsedResponse.context.Add("not_mask_number", null);
                parsedResponse.context.Add("places_location", null);
                parsedResponse.context.Add("opciones_ingreso", null);
                parsedResponse.context.Add("contador_intentos", 0);
                parsedResponse.context.Add("tipo_localizacion", null);
                parsedResponse.context.Add("opciones_beneficio", null);
                parsedResponse.context.Add("cancelar_tipo_seguro", null);
                parsedResponse.context.Add("locacion_punto_contacto", null);
                parsedResponse.context.Add("nid", "in-9");


                return parsedResponse;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
    }
}
