﻿namespace middleware2.Componentes
{
    public class Carousel
    {
        public AssistantMessageResponse loadCarousel()
        {
            AssistantMessageResponse parsedResponse = new AssistantMessageResponse();
            parsedResponse.response = new List<MessageTextAudio>() { new MessageTextAudio() { text = "Respuesta" } };
            parsedResponse.intents = new List<RuntimeIntent>() { new RuntimeIntent() { Intent = "inf_gral", Confidence = 1 } };
            parsedResponse.context = new Dictionary<string, object>() { };

            List<Card> card = new List<Card>() { new Card() { title = "titulo1", description = "descripcion 1", latitude = 0,longitude=0,image_url= "https://educacion30.b-cdn.net/wp-content/uploads/2019/02/girasoles-978x652.jpg", url= "https://educacion30.b-cdn.net/wp-content/uploads/2019/02/girasoles-978x652.jpg" },
                                                 new Card() { title = "titulo2", description = "descripcion 2", latitude = 0,longitude=0,image_url= "https://www.tooltyp.com/wp-content/uploads/2014/10/1900x920-8-beneficios-de-usar-imagenes-en-nuestros-sitios-web.jpg", url= "https://www.tooltyp.com/wp-content/uploads/2014/10/1900x920-8-beneficios-de-usar-imagenes-en-nuestros-sitios-web.jpg" } };


            parsedResponse.context.Add("agente", false);
            parsedResponse.context.Add("cont", 2);
            parsedResponse.context.Add("text", null);
            parsedResponse.context.Add("today", "Miercoles");
            parsedResponse.context.Add("buscar", null);
            parsedResponse.context.Add("images", null);
            parsedResponse.context.Add("remesa", null);
            parsedResponse.context.Add("estados", null);
            parsedResponse.context.Add("new_url", null);
            parsedResponse.context.Add("loc_pass", null);
            parsedResponse.context.Add("opciones", null);
            parsedResponse.context.Add("despedida", null);
            parsedResponse.context.Add("localizar", null);
            parsedResponse.context.Add("confidence", 0.3);
            parsedResponse.context.Add("iniciativa", 30);
            parsedResponse.context.Add("time_query", null);
            parsedResponse.context.Add("duda_generica", null);
            parsedResponse.context.Add("tipos_tarjeta", null);
            parsedResponse.context.Add("not_mask_number", null);
            parsedResponse.context.Add("places_location", null);
            parsedResponse.context.Add("opciones_ingreso", null);
            parsedResponse.context.Add("contador_intentos", 0);
            parsedResponse.context.Add("tipo_localizacion", null);
            parsedResponse.context.Add("opciones_beneficio", null);
            parsedResponse.context.Add("cancelar_tipo_seguro", null);
            parsedResponse.context.Add("locacion_punto_contacto", null);
            parsedResponse.context.Add("nid", "in-9");
            parsedResponse.context.Add("carousel", new List<Card>() { new Card() { title="Prueba1", latitude=0,longitude=0, description="descripción 1", image_url= "https://educacion30.b-cdn.net/wp-content/uploads/2019/02/girasoles-978x652.jpg", url= "https://educacion30.b-cdn.net/wp-content/uploads/2019/02/girasoles-978x652.jpg" },
                                                                      new Card() { title="Prueba2", latitude=0,longitude=0, description="descripción 2", image_url= "https://www.tooltyp.com/wp-content/uploads/2014/10/1900x920-8-beneficios-de-usar-imagenes-en-nuestros-sitios-web.jpg", url= "https://www.tooltyp.com/wp-content/uploads/2014/10/1900x920-8-beneficios-de-usar-imagenes-en-nuestros-sitios-web.jpg" }});
            //parsedResponse.Location = "[{\"title\":\"Prueba1\",\"description\":\"PruebaDes1\"},{\"title\":\"Prueba2\",\"description\":\"PruebaDes2\"}]";
            //parsedResponse.Location = "[{pos1,pos2,pos3},{pos21,pos22,pos23}]";
            parsedResponse.location = System.Text.Json.JsonSerializer.Serialize(card);

            return parsedResponse;
        }
        
    }
}
