﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System;
using System.Threading.Tasks;

namespace middleware2
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;


        public CustomMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger(typeof(CustomMiddleware));
        }

        public async Task Invoke(HttpContext context)
        {
            //Este id es para simular un ID que viene del FrontEnd/Container
            //Con el cual mantenemos "trace" de toda la acción del usuario
            //Antes de la request
            Guid traceId = Guid.NewGuid();
            _logger.LogDebug($"Request {traceId} iniciada");
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();


            await _next(context);

            //Despues de la request
            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            _logger.LogDebug($"La request {traceId} ha llevado {elapsedTime} ");
        }
    }
}

