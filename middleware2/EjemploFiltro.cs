﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace middleware2
{
    public class EjemploFiltro: ActionFilterAttribute
    {
        private readonly ILogger<EjemploFiltro> _logger;

        public EjemploFiltro(ILogger<EjemploFiltro> logger)
        {
            _logger = logger;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            _logger.LogInformation($"Antes del método {context.ActionDescriptor.DisplayName}");
            await next();
            _logger.LogInformation($"Después del método {context.ActionDescriptor.DisplayName}");

        }
    }
}
