﻿namespace middleware2.Repositorios
{
    public interface IRepositorio
    {
        Task<HttpResponseWrapper<object>> Delete(string url);
        Task<HttpResponseWrapper<T>> Get<T>(string url, string token = "");
        Task<HttpResponseWrapper<object>> Post<T>(string url, T enviar, string token = "");
        Task<HttpResponseWrapper<TResponse>> Post<T, TResponse>(string url, T enviar, string token = "");
        //Task<object> Post<T, TResponse>(string url, T enviar, string token = "");
        Task<HttpResponseWrapper<object>> Put<T>(string url, T enviar);
    }
}
