﻿using middleware2.Repositorios;

namespace middleware2.Controller
{
    [ApiController]
    [Route("Prueba")]
    public class HomeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IRepositorio _repositorio;

        public HomeController(IConfiguration configuration, IRepositorio repositorio)
        {
            _configuration = configuration;
            _repositorio = repositorio;
        }

        [ServiceFilter(typeof(EjemploFiltro))]
        [HttpPost("prb")]
        public async Task<ActionResult> Post([Bind("message")][FromBody] AssistantMessageRequest messageBody)
        {            
            if (messageBody.message.ToUpper().Equals("TICKET"))
                return Ok(new Form1().loadForm1());
            else if(messageBody.message.ToUpper().Equals("COROUSEL"))
                return Ok(new Carousel().loadCarousel());
            else
                return Ok(new MessageText().MessageT(_configuration, _repositorio));
        }
    }
}
