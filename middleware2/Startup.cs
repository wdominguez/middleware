using Microsoft.Extensions.DependencyInjection;

namespace middleware2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddRazorPages();
            services.AddScoped<EjemploFiltro>();
            services.AddScoped<HttpClient>();
            services.AddScoped<IRepositorio, Repositorio>();
            services.AddScoped<HttpResponseMessage>();
            //services.AddSingleton<IRepositorio, Repositorio>();            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors();
            //app.UseCors(builder => {
            //    builder.AllowAnyHeader();
            //    builder.WithMethods(new string[] { "GET", "POST", "OPTIONS" });
            //    builder.AllowAnyOrigin();
            //});
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapRazorPages();
            //});

            app.UseMiddleware<CustomMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });




            //app.Use(async (context, next) =>
            //{
            //    await context.Response.WriteAsync("First Middleware");
            //    await next();
            //});

            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Second Middleware");
            //});
        }
    }
}
